package task3.firstSolution;

import java.util.List;
import java.util.Map;

public interface MyParser<S, D> {

    Map<S, D> parse(S s);
    Map<S, D> loadConditions(List<S> conditions);
}
