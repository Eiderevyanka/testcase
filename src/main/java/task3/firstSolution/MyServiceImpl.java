package task3.firstSolution;

import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Collectors;

public class MyServiceImpl implements MyService<String> {

    @Override
    public List<String> action(List<String> data) {

        MyParser<String, Double> parser = new MyParserImpl();
        Map<String, Double> conditions = parser.loadConditions(data);

        return data.stream()
                .filter(s -> s.contains("?"))
                .map(s -> calculate(s, conditions)
                        .map(value -> s.replace("?", value))
                        .orElse("Conversion not possible."))
                .collect(Collectors.toList());
    }

    private Optional<String> calculate(String s, Map<String, Double> map) {

        String[] strings = s.split(" ");

        Double a = Double.parseDouble(strings[0]);
        String V = strings[1];
        String W = strings[4];
        Double b = map.get(V + "/" + W);

        if (Objects.isNull(b)) {
            return Optional.empty();
        }

        DecimalFormat decimalFormat = new DecimalFormat("#.###");
        String result = decimalFormat.format(a * b);

        return Optional.of(result);
    }
}
