package task3.firstSolution;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyParserImpl implements MyParser<String, Double> {

    @Override
    public Map<String, Double> parse(String s) {
        Map<String, Double> map = new HashMap<>();
        String[] strings = s.split(" ");

        Double a = Double.parseDouble(strings[0]);
        String V = strings[1];
        Double b = Double.parseDouble(strings[3]);
        String W = strings[4];

        map.put(V + "/" + W, a / b);
        map.put(W + "/" + V, b / a);

        return map;
    }

    @Override
    public Map<String, Double> loadConditions(List<String> conditions) {

        Map<String, Double> map = new HashMap<>();
        conditions.stream()
                .filter(s -> !s.contains("?"))
                .filter(s -> !s.isEmpty())
                .forEach(s -> map.putAll(parse(s)));


        Map<String, Double> resultMap = new HashMap<>();
        map.keySet().forEach(s -> {
            String[] strings = s.split("/");
            map.keySet().stream()
                    .filter(s1 -> s1.contains(strings[0]))
                    .filter(s1 -> !s1.contains(strings[1]))
                    .forEach(s1 -> addConditions(s, s1, map, resultMap));
        });

        map.putAll(resultMap);
        return map;
    }

    private void addConditions(String s, String s1, Map<String, Double> map, Map<String, Double> result) {

        String[] strings = s.split("/");
        String[] strings1 = s1.split("/");

        if (strings[0].equals(strings1[0])) {
            result.put(strings[1] + "/" + strings1[1], map.get(s) / map.get(s1));
            result.put(strings1[1] + "/" + strings[1], map.get(s1) / map.get(s));
        } else {
            result.put(strings[1] + "/" + strings1[0], map.get(s) * map.get(s1));
            result.put(strings1[0] + "/" + strings[1], map.get(s1) * map.get(s));
        }
    }
}
