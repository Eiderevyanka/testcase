package task3.firstSolution;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Solution {

    public static void main(String[] args) {

        try (BufferedReader reader = new BufferedReader(new FileReader("testData.txt"));
             BufferedWriter writer = new BufferedWriter(new FileWriter("outData.txt"))) {

            List<String> data = new ArrayList<>();
            MyService<String> service = new MyServiceImpl();

            while (reader.ready()) {
                data.add(reader.readLine());
            }

            List<String> result = service.action(data);

            for (int i = 0; i < result.size(); i++) {
                writer.write(result.get(i));
                if (i + 1 != result.size()) {
                    writer.newLine();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
