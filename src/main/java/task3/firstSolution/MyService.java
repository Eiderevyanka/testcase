package task3.firstSolution;

import java.util.List;

public interface MyService<S> {

    List<S> action(List<S> data);
}
