package task3.secondSolution;

import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Collectors;

public class MyServiceImpl implements MyService {

    private final MyConverterImpl converter;

    public MyServiceImpl() {
        this.converter = new MyConverterImpl();
    }

    @Override
    public List<String> action(List<String> data) {

        data.stream()
                .filter(s -> !s.contains("?"))
                .filter(s -> !s.isEmpty())
                .forEach(this::createConverter);

        DecimalFormat decimalFormat = new DecimalFormat("#.###");

        return data.stream()
                .filter(s -> s.contains("?"))
                .map(s -> calculate(s)
                        .map(result -> s.replace("?", decimalFormat.format(result)))
                        .orElse("Conversion not possible."))
                .collect(Collectors.toList());
    }

    private void createConverter(String s) {

        String[] strings = s.split(" ");

        double a = Double.parseDouble(strings[0]);
        String V = strings[1];
        double b = Double.parseDouble(strings[3]);
        String W = strings[4];

        converter.addUnit(V, a, W, b);
    }

    private Optional<Double> calculate(String s) {

        String[] strings = s.split(" ");

        double a = Double.parseDouble(strings[0]);
        String V = strings[1];
        String W = strings[4];

        return converter.convert(V, W).map(result -> a * result);
    }
}
