package task3.secondSolution;

import java.util.List;

public interface MyService {

    List<String> action(List<String> data);
}
