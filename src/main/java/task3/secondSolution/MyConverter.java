package task3.secondSolution;

import java.util.Optional;

public interface MyConverter<T, D> {

    Optional<D> convert(T t, T t1);
}
