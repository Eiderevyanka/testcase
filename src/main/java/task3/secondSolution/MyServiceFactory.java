package task3.secondSolution;

public class MyServiceFactory {

    static MyService getService(String name) {
        if (name.equals("a V = b W")) {
            return new MyServiceImpl();
        } else throw new RuntimeException("bad request");
    }
}
