package task3.secondSolution;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class MyConverterImpl implements MyConverter<String, Double>{

    private final Map<String, Unit> units = new HashMap<>();

    public MyConverterImpl() {
    }

    public Map<String, Unit> getUnits() {
        return units;
    }

    public void addUnit(String V, double a, String W, double b) {

        units.putIfAbsent(W, new Unit(W));
        units.putIfAbsent(V, new Unit(V));

        units.get(V).addMultiplier(W, b / a);
        units.get(W).addMultiplier(V, a / b);
    }

    @Override
    public Optional<Double> convert(String V, String W) {

        if (!units.containsKey(V) || !units.containsKey(W)) {
            return Optional.empty();
        }

        Map<String, Double> multipliersV = units.get(V).getAllMultipliers();
        Map<String, Double> multipliersW = units.get(W).getAllMultipliers();

        for (Map.Entry<String, Double> entry : multipliersW.entrySet()) {
            String s = entry.getKey();
            Double value = entry.getValue();
            if (multipliersV.containsKey(s)) {
                return Optional.of(multipliersV.get(s) / value);
            } else {
                return Optional.empty();
            }
        }

        return Optional.empty();
    }

    private class Unit {

        private final String name;
        private final Map<String, Double> multipliers = new HashMap<>();

        public Unit(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public Map<String, Double> getAllMultipliers() {
            return multipliers;
        }

        public void addMultiplier(String name, Double multiplier) {
            this.multipliers.putIfAbsent(name, multiplier);
        }

        public void removeMultiplier(String name) {
            this.multipliers.remove(name);
        }

        public boolean isContainsMultiplier(String name) {
            return multipliers.containsKey(name);
        }

        public Double getMultiplier(String name) {
            return multipliers.get(name);
        }
    }
}
