
package task4.service;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;
import task4.dao.PersonRepository;
import task4.dao.entity.PersonEntity;
import task4.dto.Person;
import task4.error.BusinessLogicException;
import task4.error.ErrorCodes;


import java.util.*;

public class PersonServiceImpl implements PersonService {

    private PersonRepository personRepository;

    @Autowired
    public void setPersonRepository(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    /**
     * Получить список пользователей
     */
    @Override
    public List<Person> getAllPersons() {

        try {
            List<Person> persons = new ArrayList<>();
            personRepository.findAll().forEach(entity -> persons.add(convertPersonEntityToPerson(entity)));
            return persons;
        } catch (Exception e) {
            throw new BusinessLogicException(e.getMessage(), ErrorCodes.DATABASE_ERROR.toString());
        }
    }

    /**
     * Получить информацию о пользователе по номеру телефона
     *
     * @param phoneNumber
     */
    @Override
    public Person getPersonByPhone(String phoneNumber) {

        try {
            validatePhoneNumber(phoneNumber);
            if (!personRepository.existsByPhoneNumber(phoneNumber)) {
                throw new BusinessLogicException("Not found person with such phone number", ErrorCodes.PERSON_NOT_FOUND.toString());
            }
            return convertPersonEntityToPerson(personRepository.findByPhoneNumber(phoneNumber));
        } catch (BusinessLogicException e) {
            throw e;
        } catch (Exception e) {
            throw new BusinessLogicException(e.getMessage(), ErrorCodes.DATABASE_ERROR.toString());
        }
    }

    /**
     * Создать нового пользователя
     *
     * @param person
     */
    @Override
    public Person createPerson(Person person) {

        try {
            validatePerson(person);
            if (personRepository.existsByPhoneNumber(person.getPhoneNumber())) {
                throw new BusinessLogicException("Person already exists", ErrorCodes.DUPLICATE_PERSON.toString());
            }
            personRepository.save(convertPersonToPersonEntity(person));
            return person;
        } catch (BusinessLogicException e) {
            throw e;
        } catch (Exception e) {
            throw new BusinessLogicException(e.getMessage(), ErrorCodes.DATABASE_ERROR.toString());
        }
    }

    /**
     * Удалить пользователя по номеру телефона
     *
     * @param phoneNumber
     */
    @Override
    public void deletePerson(String phoneNumber) {

        try {
            validatePhoneNumber(phoneNumber);
            if (!personRepository.existsByPhoneNumber(phoneNumber)) {
                throw new BusinessLogicException("Not found person with such phone number", ErrorCodes.PERSON_NOT_FOUND.toString());
            }
            personRepository.delete(personRepository.findByPhoneNumber(phoneNumber));
        } catch (BusinessLogicException e) {
            throw e;
        } catch (Exception e) {
            throw new BusinessLogicException(e.getMessage(), ErrorCodes.DATABASE_ERROR.toString());
        }
    }

    /**
     * Изменить пользователя
     *
     * @param person
     */
    @Override
    public Person updatePerson(Person person) {

        try {
            validatePerson(person);
            if (!personRepository.existsByPhoneNumber(person.getPhoneNumber())) {
                throw new BusinessLogicException("Not found person with such phone number", ErrorCodes.PERSON_NOT_FOUND.toString());
            }
            deletePerson(person.getPhoneNumber());
            personRepository.save(convertPersonToPersonEntity(person));
            return person;
        } catch (BusinessLogicException e) {
            throw e;
        } catch (Exception e) {
            throw new BusinessLogicException(e.getMessage(), ErrorCodes.DATABASE_ERROR.toString());
        }
    }

    /**
     * Загрузить пользователей из файла
     *
     * @param file
     */
    @Override
    public List<Person> uploadPersons(MultipartFile file) {

        try {
            if (!Objects.requireNonNull(file.getOriginalFilename()).toLowerCase().endsWith(".csv")) {
                throw new BusinessLogicException("Wrong file format", ErrorCodes.ILLEGAL_ARGUMENT.toString());
            }

            List<Person> people;
            try {
                CsvSchema bootstrapSchema = CsvSchema.emptySchema().withHeader();
                CsvMapper mapper = new CsvMapper();
                MappingIterator<Person> readValues =
                        mapper.reader(Person.class).with(bootstrapSchema).readValues(file.getBytes());
                people = readValues.readAll();
            } catch (Exception e) {
                throw new BusinessLogicException(e.getMessage(), ErrorCodes.LOAD_FROM_FILE_ERROR.toString());
            }

            people.forEach(person -> {
                validatePhoneNumber(person.getPhoneNumber());
                if (personRepository.existsByPhoneNumber(person.getPhoneNumber())) {
                    updatePerson(person);
                } else {
                    createPerson(person);
                }
            });

            return people;
        } catch (BusinessLogicException e) {
            throw e;
        } catch (Exception e) {
            throw new BusinessLogicException(e.getMessage(), ErrorCodes.DATABASE_ERROR.toString());
        }
    }

    private Person convertPersonEntityToPerson(PersonEntity entity) {

        return Person.builder()
                .firstName(entity.getFirstName())
                .lastName(entity.getLastName())
                .phoneNumber(entity.getPhoneNumber())
                .address(entity.getAddress())
                .build();
    }

    private PersonEntity convertPersonToPersonEntity(Person person) {

        return PersonEntity.builder()
                .firstName(person.getFirstName())
                .lastName(person.getLastName())
                .phoneNumber(person.getPhoneNumber())
                .address(person.getAddress())
                .build();
    }

    private void validatePerson(Person person) {

        if (Objects.isNull(person)) {
            throw new BusinessLogicException("Person can't be null",
                    ErrorCodes.ILLEGAL_ARGUMENT.toString());
        }

        String firstName = person.getFirstName();
        String lastName = person.getLastName();
        String phoneNumber = person.getPhoneNumber();
        String address = person.getAddress();

        if (Objects.isNull(firstName) || firstName.equals("")
                || Objects.isNull(lastName) || lastName.equals("")
                || Objects.isNull(phoneNumber) || phoneNumber.equals("")
                || Objects.isNull(address) || address.equals("")) {
            throw new BusinessLogicException("Person's fields can't be null or empty",
                    ErrorCodes.ILLEGAL_ARGUMENT.toString());
        }
    }

    private void validatePhoneNumber(String phoneNumber) {

        if (Objects.isNull(phoneNumber) || phoneNumber.isEmpty()) {
            throw new BusinessLogicException("Phone number can't be null or empty",
                    ErrorCodes.ILLEGAL_ARGUMENT.toString());
        }
    }
}
