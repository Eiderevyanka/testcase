package task4.service;

import org.springframework.web.multipart.MultipartFile;
import task4.dto.Person;

import java.util.List;

public interface PersonService {

    /**
     * Получить список пользователей
     */
    List<Person> getAllPersons();

    /**
     * Получить информацию о пользователе по номеру телефона
     *
     * @param phoneNumber
     */
    Person getPersonByPhone(String phoneNumber);

    /**
     * Создать нового пользователя
     *
     * @param person
     */
    Person createPerson(Person person);

    /**
     * Удалить пользователя по номеру телефона
     *
     * @param phoneNumber
     */
    void deletePerson(String phoneNumber);

    /**
     * Изменить пользователя
     *
     * @param person
     */
    Person updatePerson(Person person);

    /**
     * Загрузить пользователей из файла
     */
    List<Person> uploadPersons(MultipartFile file);
}
