package task4.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;

@Component
@Aspect
public class LoggingAspect {

    private static final Logger logger = Logger.getLogger(LoggingAspect.class.getName());

    @Pointcut("within(task4.service.PersonServiceImpl)")
    public void logProcessingMethod() {
    }

    @Before("logProcessingMethod()")
    public void logBefore(JoinPoint joinPoint) {

        String methodName = joinPoint.getSignature().getName();
        logger.log(Level.INFO, "Method=" + methodName + " started. Time: " + LocalDateTime.now());
    }

    @AfterReturning("logProcessingMethod()")
    public void logAfterReturning(JoinPoint joinPoint) {

        String methodName = joinPoint.getSignature().getName();
        logger.log(Level.INFO, "Method=" + methodName + " finished. Time: " + LocalDateTime.now());
    }

    @AfterThrowing("logProcessingMethod()")
    public void logAfterThrowing(JoinPoint joinPoint) {

        String methodName = joinPoint.getSignature().getName();
        logger.log(Level.WARNING, "Method=" + methodName + " failed. Time: " + LocalDateTime.now());
    }
}
