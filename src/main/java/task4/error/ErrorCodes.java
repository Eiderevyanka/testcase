package task4.error;

public enum ErrorCodes {
    ILLEGAL_ARGUMENT,
    DUPLICATE_PERSON,
    PERSON_NOT_FOUND,
    DATABASE_ERROR,
    LOAD_FROM_FILE_ERROR
}
