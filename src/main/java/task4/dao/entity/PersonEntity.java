package task4.dao.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "People")
public class PersonEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "address")
    private String address;

    public PersonEntity() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "MyEntity{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", address='" + address + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonEntity myEntity = (PersonEntity) o;
        return id == myEntity.id && firstName.equals(myEntity.firstName) && lastName.equals(myEntity.lastName) && phoneNumber.equals(myEntity.phoneNumber) && address.equals(myEntity.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, phoneNumber, address);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private final PersonEntity personEntity;

        private Builder() {
            personEntity = new PersonEntity();
        }

        public Builder id(int id) {
            personEntity.id = id;
            return this;
        }

        public Builder firstName(String firstName) {
            personEntity.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            personEntity.lastName = lastName;
            return this;
        }

        public Builder phoneNumber(String phoneNumber) {
            personEntity.phoneNumber = phoneNumber;
            return this;
        }

        public Builder address(String address) {
            personEntity.address = address;
            return this;
        }

        public PersonEntity build() {
            return personEntity;
        }
    }
}
