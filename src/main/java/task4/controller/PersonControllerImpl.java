package task4.controller;//package task4.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.sqlite.SQLiteException;
import task4.dto.Person;
import task4.error.BusinessLogicException;
import task4.error.ErrorCodes;
import task4.service.PersonService;
import task4.service.PersonServiceImpl;


import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "persons")
public class PersonControllerImpl implements PersonController {

    private PersonService personService;

    @Autowired
    public void setPersonService(PersonService personService) {
        this.personService = personService;
    }

    @Bean
    public PersonService personService() {
        return new PersonServiceImpl();
    }

    /**
     * Получить список пользователей
     */
    @Override
    @GetMapping("/all")
    public List<Person> getAllPersons() {
        return personService.getAllPersons();
    }

    /**
     * Получить информацию о пользователе по номеру телефона
     *
     * @param phoneNumber
     */
    @Override
    @GetMapping("/person")
    public Person getPersonByPhone(@RequestParam("phoneNumber") String phoneNumber) {
        return personService.getPersonByPhone(phoneNumber);
    }

    /**
     * Создать нового пользователя
     *
     * @param person
     */
    @Override
    @PostMapping("/person")
    public Person createPerson(@RequestBody Person person) {
        return personService.createPerson(person);
    }

    /**
     * Удалить пользователя по номеру телефона
     *
     * @param phoneNumber
     */
    @Override
    @DeleteMapping("/person")
    public void deletePerson(@RequestParam("phoneNumber") String phoneNumber) {
        personService.deletePerson(phoneNumber);
    }

    /**
     * Изменить пользователя
     *
     * @param person
     */
    @Override
    @PutMapping(value = "/person")
    public Person updatePerson(@RequestBody Person person) {
        return personService.updatePerson(person);
    }

    /**
     * Загрузить пользователей из файла
     *
     * @return Списиок загруженных в БД пользователей
     */
    @Override
    @PostMapping("/upload")
    public List<Person> uploadPersons(@RequestParam MultipartFile file) {
        return personService.uploadPersons(file);
    }

    @ExceptionHandler(BusinessLogicException.class)
    public ResponseEntity<Map<String, String>> handleException(BusinessLogicException ex, HttpServletRequest request) {

        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        Map<String, String> map = new HashMap<>();

        map.put("errorCode", ex.getCode());
        map.put("message", ex.getMessage());
        map.put("timestamp", LocalDateTime.now().toString());
        map.put("status", String.valueOf(status.value()));
        map.put("error", status.getReasonPhrase());
        map.put("path", request.getRequestURI());

        return new ResponseEntity<>(map, status);
    }
}
