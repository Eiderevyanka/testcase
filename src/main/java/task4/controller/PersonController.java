package task4.controller;

import org.springframework.web.multipart.MultipartFile;
import task4.dto.Person;

import java.io.File;
import java.util.List;

public interface PersonController {

    /**
     * Получить список пользователей
     */
    List<Person> getAllPersons();

    /**
     * Получить информацию о пользователе по номеру телефона
     */
    Person getPersonByPhone(String phoneNumber);

    /**
     * Создать нового пользователя
     */
    Person createPerson(Person person);

    /**
     * Удалить пользователя по номеру телефона
     */
    void deletePerson(String phoneNumber);

    /**
     * Изменить пользователя
     */
    Person updatePerson(Person person);


    /**
     * Загрузить пользователей из файла
     */
    List<Person> uploadPersons(MultipartFile file);
}
