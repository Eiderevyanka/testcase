package task1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;

public class Solution1 {
    public static void main(String[] args) {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            while (true) {
                System.out.println("Введите целое число больше 0 либо exit для завершения работы");
                String s = reader.readLine();
                if (Objects.isNull(s) || s.equals("exit")) {
                    break;
                }
                try {
                    int n = Integer.parseInt(s);
                    if (n <= 0) {
                        System.out.println("Введено число меньшее либо равное 0");
                    } else {
                        fooBar(n);
                    }
                } catch (NumberFormatException e) {
                    System.out.println("Введено не целое число");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void fooBar(int n) {
        for (int i = 1; i <= n; i++) {
            if (i % 3 == 0 && i % 5 == 0) {
                System.out.println("FooBar");
                continue;
            }
            if (i % 3 == 0) {
                System.out.println("Foo");
                continue;
            }
            if (i % 5 == 0) {
                System.out.println("Bar");
                continue;
            }
            System.out.println(i);
        }
    }
}
