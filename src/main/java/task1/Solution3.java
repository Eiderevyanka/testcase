package task1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution3 {

    public static void main(String[] args) {

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {

            int n = Integer.parseInt(reader.readLine());

            for (int i = 1; i <= n; i++) {
                boolean fooBar = i % 3 == 0 && i % 5 == 0;
                boolean foo = i % 3 == 0;
                boolean bar = i % 5 == 0;
                System.out.println(fooBar ? "FooBar" : foo ? "Foo" : bar ? "Bar" : i);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
