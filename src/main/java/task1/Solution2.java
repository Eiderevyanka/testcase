package task1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;

public class Solution2 {
    public static void main(String[] args) {
        String pattern = "^[1-9]\\d*$";
        String message = "Введите целое число больше 0";
        String result = myKeyboardReader(message, pattern);
        if (Objects.nonNull(result)) {
            fooBar(Integer.parseInt(result));
        }
    }

    public static void fooBar(int n) {
        if (n > 0) {
            fooBar(n - 1);
            if (n % 3 == 0 && n % 5 == 0) {
                System.out.println("FooBar");
            } else if (n % 3 == 0) {
                System.out.println("Foo");
            } else if (n % 5 == 0) {
                System.out.println("Bar");
            } else {
                System.out.println(n);
            }
        }
    }

    public static String myKeyboardReader(String message, String pattern) {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            while (true) {
                System.out.println(message + " Либо exit для завершения работы");
                String line = reader.readLine();
                if (Objects.isNull(line) || line.equalsIgnoreCase("exit")) {
                    return null;
                }
                if (line.matches(pattern)) {
                    return line;
                }
                System.out.println("Введено неверное значение\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
