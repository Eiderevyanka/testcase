package task2;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class MyService {

    public static void writeFile(String fileName, int data) {

        try (PrintWriter writer = new PrintWriter(new FileOutputStream(fileName))) {
            writer.print(data);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static String readFirsLineFromFile(String fileName) {
        String data = null;

        try (BufferedReader fileReader = new BufferedReader(new FileReader(fileName))) {

            if (fileReader.ready()) {
                data = fileReader.readLine();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }

    public int readFromKeyboard() {

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {

            while (true) {
                System.out.println("Введите целое число больше 0 и кратное 2, либо exit для завершения работы");
                String line = reader.readLine();

                if (Objects.isNull(line) || line.equals("exit")) {
                    return -1;
                }

                if (checkLine(line) && Integer.parseInt(line) != 0 && Integer.parseInt(line) % 2 == 0) {
                    return Integer.parseInt(line);
                } else {
                    System.out.println("Введено неверное значение");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public boolean checkLine(String line) {

        if (Objects.isNull(line) || line.length() == 0) {
            return false;
        }
        char[] chars = line.toCharArray();
        for (char c : chars) {
            if (!Character.isDigit(c)) {
                return false;
            }
        }
        return Integer.parseInt(line) != 0 || Integer.parseInt(line) % 2 == 0;
    }

    public void work(String filename) {

        int number = readFromKeyboard();

        if (number > 0) {
            writeFile(filename, 0);
            List<Future<?>> futures = new ArrayList<>();
            ExecutorService executorService = Executors.newFixedThreadPool(2);

            for (int i = 0; i < 2; i++) {
                futures.add(executorService.submit(new MyTask(filename, number)));
            }

            for (Future<?> f : futures
            ) {
                try {
                    f.get();
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            }

            executorService.shutdown();
            System.out.println("Содержимое файла=" + MyService.readFirsLineFromFile(filename));
        }
    }
}
