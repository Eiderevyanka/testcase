package task2;

public class MyTask implements Runnable {

    private static final Object syncObject = new Object();
    private final String fileName;
    private final int number;

    public MyTask(String fileName, int number) {
        this.fileName = fileName;
        this.number = number;
    }

    @Override
    public void run() {

        while (true) {

            synchronized (syncObject) {
                String data = MyService.readFirsLineFromFile(fileName);
                int oldNumber = Integer.parseInt(data);
                int newNumber;

                if (oldNumber < number) {
                    newNumber = oldNumber + 1;
                    System.out.println("Старое значение=" + oldNumber +
                            " Новое значение=" + newNumber +
                            " Thread id=" + Thread.currentThread().getId());
                } else {
                    break;
                }

                MyService.writeFile(fileName, newNumber);
            }
        }
    }
}
