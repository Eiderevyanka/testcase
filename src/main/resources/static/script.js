function showMainPage() {

    let mainPage = "<h1>Список пользователей</h1>";
    let items = JSON.parse(getItemList());
    mainPage += "<div class='main'><table class='table'>"
    mainPage += getHeader()

    for (let i = 0; i < items.length; ++i) {
        mainPage += createRow(i + 1, items[i])
    }

    mainPage += "</table>"
    mainPage += "</div>"

    document.getElementById("root").innerHTML = mainPage
}

function getHeader() {

    return "<tr>"
        + "<th>№</th>"
        + "<th>Имя</th>"
        + "<th>Фамилия</th>"
        + "<th>Номер телефона</th>"
        + "<th>Адрес</th>"
        + "</tr>"
}

function getItemList() {
    let http = new XMLHttpRequest();
    let url = '/persons/all'
    http.open('GET', url, false)
    http.send(null)

    return http.responseText
}

function createRow(idx, item) {

    let rowId = "row_" + idx

    return "<tr id='" + rowId + "'>"
        + "<td>" + idx + "</td>"
        + "<td>" + item.firstName + "</td>"
        + "<td>" + item.lastName + "</td>"
        + "<td>" + item.phoneNumber + "</td>"
        + "<td>" + item.address + "</td>"
        + "</tr>"
}