package task4.service;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.web.multipart.MultipartFile;
import task4.dao.PersonRepository;
import task4.dao.entity.PersonEntity;
import task4.dto.Person;
import task4.error.BusinessLogicException;
import task4.error.ErrorCodes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class PersonServiceImplTest {

    private PersonRepository personRepository = Mockito.mock(PersonRepository.class);
    private PersonServiceImpl personService = new PersonServiceImpl();
    private List<Person> people = new ArrayList<>();
    private List<PersonEntity> entities = new ArrayList<>();

    {
        personService.setPersonRepository(personRepository);
        for (int i = 1; i <= 5; i++) {
            people.add(Person.builder()
                    .firstName("firstName=" + i)
                    .lastName("lastName=" + i)
                    .phoneNumber("phoneNumber=" + i)
                    .address("address=" + i)
                    .build());
            entities.add(PersonEntity.builder()
                    .id(i)
                    .firstName("firstName=" + i)
                    .lastName("lastName=" + i)
                    .phoneNumber("phoneNumber=" + i)
                    .address("address=" + i)
                    .build());
        }
    }

    @Test
    public void getAllPersonsTestPositive() {

        Mockito.when(personRepository.findAll()).thenReturn(entities);
        List<Person> result = personService.getAllPersons();

        Mockito.verify(personRepository, Mockito.times(1)).findAll();
        assertEquals(people, result);
    }

    @Test
    public void getAllPersonsTestThrowingException() {

        Mockito.when(personRepository.findAll()).thenThrow(RuntimeException.class);

        try {
            personService.getAllPersons();
            fail();
        } catch (BusinessLogicException e) {
            assertEquals(ErrorCodes.DATABASE_ERROR.toString(), e.getCode());
            Mockito.verify(personRepository, Mockito.times(1)).findAll();
        }
    }

    @Test
    public void getPersonByPhoneTestPositive() {

        PersonEntity entity = entities.get(0);
        Mockito.when(personRepository.findByPhoneNumber(Mockito.anyString())).thenReturn(entity);
        Mockito.when(personRepository.existsByPhoneNumber(Mockito.anyString())).thenReturn(true);

        Person result = personService.getPersonByPhone(entity.getPhoneNumber());
        Person expectedPerson = people.get(0);

        assertEquals(expectedPerson, result);
        Mockito.verify(personRepository, Mockito.times(1)).findByPhoneNumber(entity.getPhoneNumber());
        Mockito.verify(personRepository, Mockito.times(1)).existsByPhoneNumber(entity.getPhoneNumber());
    }

    @Test
    public void getPersonByPhoneTestThrowingException() {

        Mockito.when(personRepository.existsByPhoneNumber(Mockito.anyString())).thenReturn(false);
        try {
            personService.getPersonByPhone("test");
            fail();
        } catch (BusinessLogicException e) {
            assertEquals(ErrorCodes.PERSON_NOT_FOUND.toString(), e.getCode());
            Mockito.verify(personRepository, Mockito.times(1)).existsByPhoneNumber("test");
        }

        Mockito.clearInvocations(personRepository);
        Mockito.when(personRepository.findByPhoneNumber(Mockito.anyString())).thenThrow(RuntimeException.class);
        try {
            personService.getPersonByPhone(null);
            fail();
        } catch (BusinessLogicException e) {
            assertEquals(ErrorCodes.ILLEGAL_ARGUMENT.toString(), e.getCode());
            Mockito.verify(personRepository, Mockito.times(0)).findByPhoneNumber(null);
            Mockito.verify(personRepository, Mockito.times(0)).existsByPhoneNumber("test");
        }

        try {
            personService.getPersonByPhone("");
            fail();
        } catch (BusinessLogicException e) {
            assertEquals(ErrorCodes.ILLEGAL_ARGUMENT.toString(), e.getCode());
            Mockito.verify(personRepository, Mockito.times(0)).findByPhoneNumber("");
            Mockito.verify(personRepository, Mockito.times(0)).existsByPhoneNumber("test");
        }
    }

    @Test
    public void createPersonTestPositive() {

        Person expectedPerson = people.get(1);
        assertEquals(expectedPerson, personService.createPerson(expectedPerson));
        Mockito.verify(personRepository, Mockito.times(1)).save(Mockito.any(PersonEntity.class));
        Mockito.verify(personRepository, Mockito.times(1)).existsByPhoneNumber(expectedPerson.getPhoneNumber());
    }

    @Test
    public void createPersonTestThrowingException() {

        try {
            personService.createPerson(null);
            fail();
        } catch (BusinessLogicException e) {
            assertEquals(ErrorCodes.ILLEGAL_ARGUMENT.toString(), e.getCode());
            Mockito.verify(personRepository, Mockito.times(0)).save(Mockito.any(PersonEntity.class));
            Mockito.verify(personRepository, Mockito.times(0)).existsByPhoneNumber(null);
        }

        try {
            Person emptyPhoneNumber = people.get(2);
            emptyPhoneNumber.setPhoneNumber("");
            personService.createPerson(emptyPhoneNumber);
            fail();
        } catch (BusinessLogicException e) {
            assertEquals(ErrorCodes.ILLEGAL_ARGUMENT.toString(), e.getCode());
            Mockito.verify(personRepository, Mockito.times(0)).save(Mockito.any(PersonEntity.class));
            Mockito.verify(personRepository, Mockito.times(0)).existsByPhoneNumber("");
        }

        PersonEntity entity = entities.get(0);
        Mockito.when(personRepository.existsByPhoneNumber(entity.getPhoneNumber()))
                .thenReturn(true);
        try {
            personService.createPerson(people.get(0));
            fail();
        } catch (BusinessLogicException e) {
            assertEquals(ErrorCodes.DUPLICATE_PERSON.toString(), e.getCode());
            Mockito.verify(personRepository, Mockito.times(0)).save(Mockito.any(PersonEntity.class));
            Mockito.verify(personRepository, Mockito.times(1)).existsByPhoneNumber(entity.getPhoneNumber());
        }
    }

    @Test
    public void deletePersonTestThrowingException() {

        Mockito.when(personRepository.existsByPhoneNumber("123"))
                .thenReturn(false);
        try {
            personService.deletePerson("123");
            fail();
        } catch (BusinessLogicException e) {
            assertEquals(ErrorCodes.PERSON_NOT_FOUND.toString(), e.getCode());
            Mockito.verify(personRepository, Mockito.times(1)).existsByPhoneNumber("123");
            Mockito.verify(personRepository, Mockito.times(0)).delete(Mockito.any(PersonEntity.class));
        }
    }

    @Test
    public void deletePersonTestPositive() {

        PersonEntity entity = entities.get(0);
        Mockito.when(personRepository.findByPhoneNumber(entity.getPhoneNumber())).thenReturn(entity);
        Mockito.when(personRepository.existsByPhoneNumber(entity.getPhoneNumber())).thenReturn(true);
        personService.deletePerson(entity.getPhoneNumber());

        Mockito.verify(personRepository, Mockito.times(1)).findByPhoneNumber(entity.getPhoneNumber());
        Mockito.verify(personRepository, Mockito.times(1)).existsByPhoneNumber(entity.getPhoneNumber());
        Mockito.verify(personRepository, Mockito.times(1)).delete(Mockito.any(PersonEntity.class));
    }

    @Test
    public void updatePersonTestPositive() {

        PersonEntity entity = entities.get(0);
        Person expectedPerson = people.get(0);
        Mockito.when(personRepository.findByPhoneNumber(Mockito.anyString())).thenReturn(entity);
        Mockito.when(personRepository.existsByPhoneNumber(Mockito.anyString())).thenReturn(true);
        Person result = personService.updatePerson(expectedPerson);

        assertEquals(expectedPerson, result);
        Mockito.verify(personRepository, Mockito.times(1)).findByPhoneNumber(expectedPerson.getPhoneNumber());
        Mockito.verify(personRepository, Mockito.times(2)).existsByPhoneNumber(expectedPerson.getPhoneNumber());
        Mockito.verify(personRepository, Mockito.times(1)).save(Mockito.any(PersonEntity.class));
        Mockito.verify(personRepository, Mockito.times(1)).delete(Mockito.any(PersonEntity.class));
    }

    @Test
    public void updatePersonThrowingException() {

        Mockito.when(personRepository.existsByPhoneNumber(Mockito.anyString()))
                .thenReturn(false);
        try {
            personService.updatePerson(people.get(0));
            fail();
        } catch (BusinessLogicException e) {
            assertEquals(ErrorCodes.PERSON_NOT_FOUND.toString(), e.getCode());
            Mockito.verify(personRepository, Mockito.times(1)).existsByPhoneNumber(Mockito.anyString());
            Mockito.verify(personRepository, Mockito.times(0)).findByPhoneNumber(Mockito.anyString());
            Mockito.verify(personRepository, Mockito.times(0)).save(Mockito.any(PersonEntity.class));
            Mockito.verify(personRepository, Mockito.times(0)).delete(Mockito.any(PersonEntity.class));
        }
    }

    @Test
    public void uploadPersonsTestPositive() {

        MultipartFile multipartFile = Mockito.mock(MultipartFile.class);
        String data = "firstName,lastName,phoneNumber,address\n"
                + "1,1,1,1\n"
                + "2,2,2,2\n"
                + "3,3,3,3\n";

        try {
            Mockito.when(multipartFile.getOriginalFilename()).thenReturn("1.csv");
            Mockito.when(multipartFile.getBytes()).thenReturn(data.getBytes());
            Mockito.when(personRepository.existsByPhoneNumber(Mockito.anyString())).thenReturn(true);
            Mockito.when(personRepository.findByPhoneNumber(Mockito.anyString())).thenReturn(entities.get(0));
            personService.uploadPersons(multipartFile);
            Mockito.verify(personRepository, Mockito.times(9)).existsByPhoneNumber(Mockito.anyString());
            Mockito.verify(personRepository, Mockito.times(3)).findByPhoneNumber(Mockito.anyString());
            Mockito.verify(personRepository, Mockito.times(3)).save(Mockito.any(PersonEntity.class));
            Mockito.verify(personRepository, Mockito.times(3)).delete(Mockito.any(PersonEntity.class));
        } catch (IOException e) {
            fail();
            e.printStackTrace();
        }

        try {
            Mockito.clearInvocations(personRepository);
            Mockito.when(multipartFile.getOriginalFilename()).thenReturn("1.csv");
            Mockito.when(multipartFile.getBytes()).thenReturn(data.getBytes());
            Mockito.when(personRepository.existsByPhoneNumber(Mockito.anyString())).thenReturn(false);
            personService.uploadPersons(multipartFile);
            Mockito.verify(personRepository, Mockito.times(6)).existsByPhoneNumber(Mockito.anyString());
            Mockito.verify(personRepository, Mockito.times(0)).findByPhoneNumber(Mockito.anyString());
            Mockito.verify(personRepository, Mockito.times(3)).save(Mockito.any(PersonEntity.class));
            Mockito.verify(personRepository, Mockito.times(0)).delete(Mockito.any(PersonEntity.class));
        } catch (IOException e) {
            fail();
            e.printStackTrace();
        }
    }

    @Test
    public void uploadPersonsThrowingException() {

        MultipartFile multipartFile = Mockito.mock(MultipartFile.class);

        try {
            Mockito.when(multipartFile.getOriginalFilename()).thenReturn("1.txt");
            personService.uploadPersons(multipartFile);
            fail();
        } catch (BusinessLogicException e) {
            assertEquals(ErrorCodes.ILLEGAL_ARGUMENT.toString(), e.getCode());
            assertEquals("Wrong file format", e.getMessage());
        }

        try {
            Mockito.when(multipartFile.getOriginalFilename()).thenReturn("1.csv");
            Mockito.when(multipartFile.getBytes()).thenThrow(IOException.class);
            personService.uploadPersons(multipartFile);
            fail();
        } catch (BusinessLogicException e) {
            assertEquals(ErrorCodes.LOAD_FROM_FILE_ERROR.toString(), e.getCode());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}