package task1;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class Solution1Test {

    private final InputStream systemIn = System.in;
    private final PrintStream systemOut = System.out;

    private ByteArrayOutputStream testOut;

    @Before
    public void setUpOutput() {
        testOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOut));
    }

    private void provideInput(String data) {
        System.setIn(new ByteArrayInputStream(data.getBytes()));
    }

    @After
    public void restoreSystemInputOutput() {
        System.setIn(systemIn);
        System.setOut(systemOut);
    }

    private String getOutput() {
        return testOut.toString();
    }

    @Test
    public void fooBarTest() {

        Solution1.fooBar(30);

        String[] strings = getOutput().split("\n");

        assertEquals("1", strings[0].trim());
        assertEquals("2", strings[1].trim());
        assertEquals("Foo", strings[2].trim());
        assertEquals("4", strings[3].trim());
        assertEquals("Bar", strings[4].trim());
        assertEquals("Foo", strings[5].trim());
        assertEquals("7", strings[6].trim());
        assertEquals("8", strings[7].trim());
        assertEquals("Foo", strings[8].trim());
        assertEquals("Bar", strings[9].trim());
        assertEquals("11", strings[10].trim());
        assertEquals("Foo", strings[11].trim());
        assertEquals("13", strings[12].trim());
        assertEquals("14", strings[13].trim());
        assertEquals("FooBar", strings[14].trim());
        assertEquals("FooBar", strings[29].trim());

    }

    @Test
    public void mainTest() {

        provideInput("exit");
        Solution1.main(new String[0]);
        assertEquals("Введите целое число больше 0 либо exit для завершения работы",
                getOutput().trim());

        provideInput("0");
        Solution1.main(new String[0]);
        String[] strings = getOutput().split("\n");
        assertEquals("Введено число меньшее либо равное 0", strings[strings.length - 2].trim());

        provideInput("qwerty");
        Solution1.main(new String[0]);
        strings = getOutput().split("\n");
        assertEquals("Введено не целое число", strings[strings.length - 2].trim());
    }
}