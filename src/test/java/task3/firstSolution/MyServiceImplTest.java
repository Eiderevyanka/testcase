package task3.firstSolution;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class MyServiceImplTest {

    @Test
    public void actionTest() {

        MyService<String> service = new MyServiceImpl();
        String s = "1024 byte = 1 kilobyte\n" +
                "2 bar = 12 ring\n" +
                "16.8 ring = 2 pyramid\n" +
                "4 hare = 1 cat\n" +
                "5 cat = 0.5 giraffe\n" +
                "1 byte = 8 bit\n" +
                "15 ring = 2.5 bar\n" +
                "\n" +
                "\n" +
                "1 pyramid = ? bar\n" +
                "1 giraffe = ? hare\n" +
                "0.5 byte = ? cat\n" +
                "2 kilobyte = ? bit";
        List<String> data = Arrays.asList(s.split("\n"));
        s = "1 pyramid = 1,4 bar\n" +
                "1 giraffe = 40 hare\n" +
                "Conversion not possible.\n" +
                "2 kilobyte = 16384 bit";
        List<String> expected = Arrays.asList(s.split("\n"));
        List<String> result = service.action(data);

        Assert.assertEquals(expected, result);
    }
}