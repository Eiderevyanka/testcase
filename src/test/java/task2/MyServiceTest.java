package task2;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

public class MyServiceTest {

    private final InputStream systemIn = System.in;
    private final PrintStream systemOut = System.out;

    private ByteArrayOutputStream testOut;

    @Before
    public void setUpOutput() {
        testOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOut));
    }

    private void provideInput(String data) {
        System.setIn(new ByteArrayInputStream(data.getBytes()));
    }

    @After
    public void restoreSystemInputOutput() {
        System.setIn(systemIn);
        System.setOut(systemOut);
    }

    private String getOutput() {
        return testOut.toString();
    }

    @Test
    public void checkLine() {

        MyService myService = new MyService();
        assertTrue(myService.checkLine("0"));
        assertTrue(myService.checkLine("1"));
        assertTrue(myService.checkLine("2"));
        assertFalse(myService.checkLine("-1"));
        assertFalse(myService.checkLine("asd"));
        assertFalse(myService.checkLine(""));
        assertFalse(myService.checkLine(null));
    }

    @Test
    public void readFromKeyboard() {

        MyService service = new MyService();

        provideInput("3");
        int number = service.readFromKeyboard();
        assertEquals(-1, number);
        String[] strings = getOutput().split("\n");
        assertEquals("Введено неверное значение", strings[1].trim());

        provideInput("100");
        number = service.readFromKeyboard();
        assertEquals(100, number);

        provideInput("exit");
        number = service.readFromKeyboard();
        assertEquals(-1, number);

        provideInput("0");
        number = service.readFromKeyboard();
        assertEquals(-1, number);
    }

    @Test
    public void work() {

        provideInput("30");
        new MyService().work("out.txt");
        String outputString = getOutput().trim();
        String actual = outputString.substring(outputString.lastIndexOf("="));

        assertEquals("=30", actual);

        for (int i = 0, j = 1; i < 30; i++, j++) {
            assertTrue(outputString.contains("Старое значение=" + i));
            assertTrue(outputString.contains("Новое значение=" + j));
        }
    }
}